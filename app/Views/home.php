<?= $this->extend('layout') ?>

<?= $this->section('css') ?>
    <!-- ========= link the CSS of this section, this block is optional  ========= -->
<?= $this->endSection() ?>


<?= $this->section('content') ?>
    <!-- ========= Content of the Page  ========= -->
    <h1 class="text-center">Bienvenido al Starter Template de Codeigniter 4.2!</h1>
<?= $this->endSection() ?>


<?= $this->section('js') ?>
    <!-- ========= link the JS for this section, this block is optional  ========= -->
<?= $this->endSection() ?>