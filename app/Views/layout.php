<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CodeInniter 4.2 Starter Template</title>

    <!-- ========= GLOBAL CSS  ========= -->
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css')?>">


    <!-- ========= CUSTOM CSS  ========= -->
    <?= $this->renderSection('css') ?>


</head>
<body>
    <!-- ========= HEADER  ========= -->
	<?= $this->include('header') ?>


    <!-- ========= PAGE CONTENT  ========= -->
    <?= $this->renderSection('content') ?>


    <!-- ========= FOOTER  ========= -->
	<?= $this->include('footer') ?>

    <!-- ========= GLOBAL JS  ========= -->
    <script src="<?= base_url('assets/js/app.js')?>"></script>


    <!-- ========= CUSTOM JS  ========= -->
    <?= $this->renderSection('js') ?>
</body>
</html>